# Clean Resurrected dokuwiki template

* Designed by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information and colour themes](http://dokuwiki.org/template:cleanresurrected)

![cleanresurrected theme light screenshot](https://i.imgur.com/KhUBTbm.png)

![cleanresurrected theme dark screenshot](https://i.imgur.com/jE8Ybu8.png)